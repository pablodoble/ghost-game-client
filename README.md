# Ghost game client

## Getting started

(To make it work, server must be running)

Open a terminal and go to the repo path. Then:

```
npm i
npm start
```

Application will be running on http://localhost:4200